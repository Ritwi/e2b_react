import React, { Component } from 'react';
import './Login.css';
import {
  BrowserRouter,
  Redirect,
} from "react-router-dom";

class App extends Component {
  state = {
    email: "ritwika3@gmail.com",
    password: "password"
  };

  login() {
  console.log('abc');
  fetch('http://localhost:8000/login', {
      method: 'post',
      body: JSON.stringify({
        id : this.state.email,
        password : this.state.password
      }),
      headers : {
          'Content-Type': 'application/json',
        },
    }).then(response => response.json())
    .then(response => {

    var data = response;
    console.log(data);
    if (data.code===1) {
        console.log(data.code);
        return <Redirect to='/login'/>;
      }
   });
  }

  render() {
    return (
      <div>
        <br/><br/>
        Email : <input type="text" value={this.state.email} onChange={ this.handleEmail.bind(this) } /><br/><br/>
        Password : <input type="password" value={this.state.password} onChange={ this.handlePassword.bind(this) } /><br/><br/>
        <button onClick={this.login.bind(this)}>LOGIN</button>
      </div>
    );
  }

  handleEmail(e) {

        this.setState({ email: e.target.value });
  }
  handlePassword(e) {
        this.setState({ password: e.target.value });
  }
}

export default App;
