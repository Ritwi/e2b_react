import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter,
  Redirect,
} from "react-router-dom";

class App extends Component {

  login(event) {
    event.preventDefault();
    browserHistory.push('/login');
    return <Redirect to='/login'/>;
    console.log(this.refs);
    let email = this.refs.email.value;
    let password = this.refs.password.value;
    let data = {
      email,
      password,
    }
    let datas = this.state.datas;
    datas.push(data);

    this.setState({
      datas : datas
    })
  }

  constructor(){
    super();
    this.login = this.login.bind(this);
    this.state = {
      email : '',
      password : '',
      datas: []
    }
  }

  render() {
    let data = this.state.datas;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">E2B Project Login</h1>
        </header>
        <br/><br/>
        <form>
          Email : <input type="text" ref="email" placeholder="email"/><br/><br/>
          Password : <input type="password" ref="password" placeholder="password" /><br/><br/>
          <button onClick={this.login}>LOGIN</button>
        </form>
        <pre>
          {JSON.stringify(data)}
        </pre>
      </div>
    );
  }

}

export default App;
